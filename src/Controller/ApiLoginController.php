<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ApiLoginController extends AbstractController
{
    /**
     * @Route("/api/login", name="app_api_login")
    */
    public function index(UserInterface $user): Response
    {
        $this->getUser();
       if (null === $user)
       {
           return $this->json(
           [
               'message' => 'missing credentials',
           ], Response::HTTP_UNAUTHORIZED);
       }

       $token = "token"; // somehow create an API token for $user

       return $this->json(
           [
           'user'  => $user->getUserIdentifier(),
           'token' => $token,
           ]);
    }
}
