<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"id":"partial", "name": "ipartial", "description":"ipartial", "price":"partial", "quality":"ipartial"})
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quality;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    function getName(): ?string
    {
        return $this->name;
    }

   function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    function getDescription(): ?string
    {
        return $this->description;
    }

    function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    function getPrice(): ?int
    {
        return $this->price;
    }

    function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }
    

    public function getQuality(): ?string
    {
        return $this->quality;
    }

    public function setQuality(string $quality): self
    {
        $this->quality = $quality;

        return $this;
    }
}